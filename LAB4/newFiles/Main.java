import Parser.*;
import Lexer.*;
import java.io.*;
import AST.*;

public class Main{
  public static void main(String args[]) throws Exception{
    java.io.BufferedReader in;
    Yylex sc;
    parser p;
    java_cup.runtime.Symbol sroot;
    Start root = null;
    boolean error=false;

    //El primer parametro es el nombre del fichero con el programa
    if (args.length < 1) {
      System.out.println(
        "Uso: java Main <nombre_fichero>");
      error=true;
    }

    //Analisis lexico y sintactico

    if (!error) {
	try {
	    in = new java.io.BufferedReader(new java.io.FileReader(args[0]));
	    sc = new Yylex(in);
	    p = new parser(sc);
	    sroot = p.parse();
	    System.out.println("Analisis lexico y sintactico correctos");
	    root=(Start)sroot.value;
	    root.computeAH1();
	    root.computeStTyp();
	} catch(IOException e) {
	    System.out.println("Error abriendo fichero: " + args[0]);
	    error= true;
	}
    }
    
    //Generacion de Codigo
    
    String camino = args[0];
	String camino2 = camino.replace("\\", ":");
	camino2 = camino2.replace(".", ":");
	String [] classNameArr=camino2.split(":");
    String name=classNameArr[classNameArr.length-2];
	
    //codeGenerator(name, root);
    codeGenerator(root);

    
    
  }
  private static void codeGenerator (/*String className, */Start ast) {
	  try {
		  //BufferedWriter w= new BufferedWriter(new FileWriter(className+".java"));
		  //w.write("import GeneratedCodeLib.*;");
		  //w.newLine();
		  //w.write("import java.util.*;");

		  //		  w.newLine();w.newLine();
//		  w.write("public class "+className+" {");
//		  w.newLine();
		  ast.generateCode();
//		  w.write("}");
		  //w.newLine();
		  //w.flush();
		 // w.close();
	  }catch (IOException e) {}
  }
}