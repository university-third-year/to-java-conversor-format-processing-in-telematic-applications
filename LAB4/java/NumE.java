import GeneratedCodeLib.*;
import java.util.*;

public class NumE {
 public static void nume (int n) {
  int i;
  Rational e;
  Rational term;
  i=1;
  e=Rational.calcDiv(1,1);
  term=Rational.calcDiv(1,1);
  if(n > 0){
   do{
    term=Rational.calcDiv(term,i);
    i=i+1;
    e=Rational.calcSumaRat(e,term);

   } while(!(i > n));
   System.out.println("e~ "+Rational.q2str(e));

  }
 }
 public static void main (String []args){
  int nVars=1;
  int[] params=new int[nVars];
  if(nVars<=args.length){
   for(int i=0;i<params.length;i++){
    params[i]=Integer.parseInt(args[i]);
   }
  }else{
   for(int i=0;i<args.length;i++){
    params[i] = Integer.parseInt(args[i]);
   }
   for(int i=args.length; i<nVars; i++){
    params[i] = 0;
   }
  }
  nume(params[0]);
 }
}
