import GeneratedCodeLib.*;
import java.util.*;

public class Ejem1 {
 public static void ejem1 (int a,int b) {
  int c;
  Rational d;
  c=a+b;
  d=Rational.calcDiv(a,b);
  System.out.println(String.valueOf(a)+" + "+String.valueOf(b)+"= "+String.valueOf(c));
  System.out.println(String.valueOf(a)+" / "+String.valueOf(b)+"= "+Rational.q2str(d));
  System.out.println(String.valueOf(a)+" / "+String.valueOf(b)+"= "+Rational.q2strd(d,3));
 }
 public static void main (String []args){
  int nVars=2;
  int[] params=new int[nVars];
  if(nVars<=args.length){
   for(int i=0;i<params.length;i++){
    params[i]=Integer.parseInt(args[i]);
   }
  }else{
   for(int i=0;i<args.length;i++){
    params[i] = Integer.parseInt(args[i]);
   }
   for(int i=args.length; i<nVars; i++){
    params[i] = 0;
   }
  }
  ejem1(params[0],params[1]);
 }
}
