package AST;
import java.io.*;
import Errors.*;

public interface Start{
	public void computeAH1() throws CompilerExc;
	public void computeStTyp() throws CompilerExc;
	public void generateCode(BufferedWriter w) throws IOException;
}