package AST;
import Errors.*;
import java.io.*;
import Compiler.*;

public class Start1 implements Start{
	public final String i;
	public final IdentList il;
	public final LDecl ld;
	public final Body b;
	
	public Start1(String i,IdentList il, LDecl ld, Body b){
		this.i=i;
		this.il=il;
	 	this.ld=ld;
		this.b=b;
	}
	public void computeAH1() throws CompilerExc{
		il.computeAH1(Typ.tint);
		ld.computeAH1();
	}
	public void computeStTyp() throws CompilerExc{
		b.computeStTyp();
	}
	public void generateCode(BufferedWriter w) throws IOException{
		int nVars=il.nVars(0);
		w.write("public class "+i+" {");
		w.newLine();
		w.write(" public static void "+i.toLowerCase()+" ("+il.generateArguments()+") {");
		w.newLine();
		ld.generateLocalVariables(w);
		b.generateCode(w," ");
		w.write(" }");
		w.newLine();
		w.write(" public static void main (String []args){");
		w.newLine();
		w.write("  int nVars="+String.valueOf(nVars)+";");
		w.newLine();
		w.write("  int[] params=new int[nVars];");
		w.newLine();
		w.write("  if(nVars<=args.length){");
		w.newLine();
		w.write("   for(int i=0;i<params.length;i++){");
		w.newLine();
		w.write("    params[i]=Integer.parseInt(args[i]);");
		w.newLine();
		w.write("   }");
		w.newLine();
		w.write("  }else{");
		w.newLine();
		w.write("   for(int i=0;i<args.length;i++){");
		w.newLine();
		w.write("    params[i] = Integer.parseInt(args[i]);");
		w.newLine();
		w.write("  }");
		w.newLine();
		w.write("   for(int i=args.length; i<nVars; i++){");
		w.newLine();
		w.write("    params[i] = 0;");
		w.newLine();
		w.write("   }");
		w.newLine();
		w.write("  }");
		w.newLine();
		w.write("  "+i.toLowerCase()+"(");
		for(int i=0;i<nVars;i++) {
			if(i==nVars-1) {
				w.write("params["+String.valueOf(i)+"]");
			}else {
				w.write("params["+String.valueOf(i)+"],");
			}
		}
		w.write(");");
		w.newLine();
		w.write(" }");
		w.newLine();
		w.write("}");
	}
}