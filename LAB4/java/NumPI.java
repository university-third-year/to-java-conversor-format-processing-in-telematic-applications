import GeneratedCodeLib.*;
import java.util.*;

public class NumPI {
 public static void numpi (int n) {
  int i;
  int j;
  Rational pim;
  Rational term;
  i=1;
  pim=Rational.calcDiv(1,1);
  if(n > 0){
   do{
    j=1;
    term=Rational.calcDiv(1,(2)*(i)+1);
    do{
     term=Rational.calcMultRat(term,Rational.calcDiv(i+j,(j)*(4)));
     j=j+1;

    } while(!(j > i));
    i=i+1;
    pim=Rational.calcSumaRat(pim,term);

   } while(!(i > n));
   System.out.println("pi~ "+Rational.q2str(Rational.calcMultRat(pim,2)));

  }
 }
 public static void main (String []args){
  int nVars=1;
  int[] params=new int[nVars];
  if(nVars<=args.length){
   for(int i=0;i<params.length;i++){
    params[i]=Integer.parseInt(args[i]);
   }
  }else{
   for(int i=0;i<args.length;i++){
    params[i] = Integer.parseInt(args[i]);
   }
   for(int i=args.length; i<nVars; i++){
    params[i] = 0;
   }
  }
  numpi(params[0]);
 }
}
