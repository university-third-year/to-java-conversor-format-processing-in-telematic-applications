package AST;
import Compiler.*;
import Errors.*;

public class Opuesto implements Exp{

	public final Exp e;
	
	public Opuesto(Exp e){
		this.e=e;
	}
	public int computeType() throws CompilerExc{
		if(e.computeType()==Typ.tint) {
			return Typ.tint;
		}else if(e.computeType()==Typ.trat) {
			return Typ.trat;
		}else {
			throw new TypeException("Error en opuesto: la expresión no es de tipo int o rational.");
		}
	}
	public String generateCode() throws CompilerExc{
		if(e.computeType()==Typ.trat) {
			return "Rational.calcOpuestoRat(" + e.generateCode() + ")";
		}else {
			return "- ("+e.generateCode()+")";
		}
	}
}