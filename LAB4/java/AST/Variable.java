package AST;
import Errors.*;
import Compiler.*;

public class Variable implements Exp{

	public final String i;
	
	public Variable(String i){
		this.i=i;
	}
	public int computeType() throws CompilerExc{
		return SymbolTable.getType(i);
	}
	public String generateCode() throws CompilerExc{
		return i;
	}
}