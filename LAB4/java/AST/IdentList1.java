package AST;
import Errors.*;
import java.io.*;
import Compiler.*;

public class IdentList1 implements IdentList{

	public final String i;
	private int ah1;
	
	public IdentList1(String i){
		this.i=i;
	}
	public void computeAH1(int type) throws CompilerExc{
		ah1=type;
		SymbolTable.newEntry(i,type);
	}
	public int getAH1() {return ah1;}
	
	public String generateArguments() {
		return Typ.typToString(ah1)+" "+i;
	}
	public void generateLocalVariables(BufferedWriter w) throws IOException{
		if(ah1 == Typ.trat) {
			w.write("  Rational "+i+";");
			w.newLine();
		}else if(ah1 == Typ.tbool) {
			w.write("  boolean "+i+";");
			w.newLine();
		}else if(ah1 == Typ.tstr) {
			w.write("  String "+i+";");
			w.newLine();
		}else {
			w.write("  "+Typ.typToString(ah1)+" "+i+";");
			w.newLine();
		}
	}
	public int nVars(int n) {
		return n+1;
	}
}