package AST;
import Errors.*;
import java.io.*;

public class StatementList2 implements StatementList{
	public final Statement s;
	public final StatementList sl;

	
	public StatementList2(Statement s, StatementList sl){
		this.s=s;
		this.sl=sl;
	}
	public void computeStTyp() throws CompilerExc{
		s.computeStTyp();
		sl.computeStTyp();
	}
	public void generateCode(BufferedWriter w,String indent) throws IOException{
		s.generateCode(w,indent);
		sl.generateCode(w,indent);
	}
}
