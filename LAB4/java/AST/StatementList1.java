package AST;
import Errors.*;
import java.io.*;

public class StatementList1 implements StatementList{
	public final Statement s;

	
	public StatementList1(Statement s){
		this.s=s;
	}
	public void computeStTyp() throws CompilerExc{
		s.computeStTyp();
	}
	
	public void generateCode(BufferedWriter w,String indent) throws IOException{
		s.generateCode(w,indent);
	}
}