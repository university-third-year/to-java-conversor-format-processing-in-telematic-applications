package AST;
import java.io.*;
import Errors.*;

public interface Statement{
	public void computeStTyp() throws CompilerExc;
	public void generateCode(BufferedWriter w, String indent) throws IOException;
}