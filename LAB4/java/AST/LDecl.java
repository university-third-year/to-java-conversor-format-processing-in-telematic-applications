package AST;
import java.io.*;
import Errors.*;

public interface LDecl{
	public void computeAH1() throws CompilerExc;
	public void generateLocalVariables(BufferedWriter w) throws IOException;
}