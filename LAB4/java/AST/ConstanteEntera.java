package AST;
import Errors.*;
import Compiler.*;
import java.io.*;

public class ConstanteEntera implements Exp{
	public final int n;

	public ConstanteEntera(int n){
		this.n=n;
	}
	public int computeType() throws CompilerExc{
		return Typ.tint;
	}
	public String generateCode() throws CompilerExc{
		return String.valueOf(n);
	}
}