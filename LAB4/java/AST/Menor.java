package AST;
import Compiler.Typ;
import Errors.*;

public class Menor implements Exp{
	public final Exp e1, e2;


	public Menor(Exp e1, Exp e2){
		this.e1=e1;
		this.e2=e2;
	}
	public int computeType() throws CompilerExc{
		if(((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.tint))||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.trat))||((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.trat))||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.tint))) {
			return Typ.tbool;
		}else {
			throw new TypeException("Error en menor que: conflicto de tipo entre operandos.");
		}
	}
	
	public String generateCode() throws CompilerExc{
		if((e1.computeType()==Typ.trat)||(e2.computeType()==Typ.trat)) {
			return "Rational.menorRat("+e1.generateCode()+","+e2.generateCode()+")";
		}else {
			return e1.generateCode()+ " < " +e2.generateCode();
		}
	}

}