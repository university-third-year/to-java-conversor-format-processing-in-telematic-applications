package AST;
import Compiler.Typ;
import Errors.*;

public class Numerador implements Exp{

	public final Exp e;

	public Numerador(Exp e){
		this.e=e;
	}
	public int computeType() throws CompilerExc{
		if(e.computeType()==Typ.trat) {
			return Typ.tint;
		}else {
			throw new TypeException("Error en numerador: la expresión no es de tipo rational.");
		}
	}
	public String generateCode() throws CompilerExc{
		return "Rational.numerador("+e.generateCode()+")";
	}
}