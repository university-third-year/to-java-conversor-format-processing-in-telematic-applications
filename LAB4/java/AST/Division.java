package AST;
import Compiler.*;
import java.io.*;
import Errors.*;

public class Division implements Exp{

	public final Exp e1,e2;

	public Division(Exp e1, Exp e2){
		this.e1=e1;
		this.e2=e2;
	}
	public int computeType() throws CompilerExc{
		if(((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.tint))||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.trat))||((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.trat))||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.tint))) {
			return Typ.trat;
		}else {
			throw new TypeException("Error en divisi�n: conflicto de tipo entre operandos.");
		}
	}
	public String generateCode() throws CompilerExc{
		return "Rational.calcDiv("+e1.generateCode()+","+e2.generateCode()+")";
	}
}