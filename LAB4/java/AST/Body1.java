package AST;
import Errors.*;
import java.io.*;

public class Body1 implements Body{
	public final StatementList sl;

	public Body1(StatementList sl){
		this.sl=sl;
	}
	public void computeStTyp() throws CompilerExc{
		sl.computeStTyp();
	}
	public void generateCode(BufferedWriter w, String indent) throws IOException{
		sl.generateCode(w,indent);
	}
}