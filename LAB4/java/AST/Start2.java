package AST;
import Errors.*;
import java.io.*;

public class Start2 implements Start{
	public final String i;
	public final LDecl ld;
	public final Body b;
	
	public Start2(String i, LDecl ld, Body b){
		this.i=i;
		this.ld=ld;
		this.b=b;
	}
	public void computeAH1() throws CompilerExc{
		ld.computeAH1();
	}

	public void computeStTyp() throws CompilerExc{
		b.computeStTyp();
	}
	public void generateCode() throws IOException{
		try {
		BufferedWriter w= new BufferedWriter(new FileWriter(i+".java"));
		w.write("import GeneratedCodeLib.*;");
		w.newLine();
		w.write("import java.util.*;");
		w.newLine();
		w.newLine();
		
		w.write("public class "+i+" {");
		w.newLine();
		w.write(" public static void "+i.toLowerCase()+" () {");
		w.newLine();
		ld.generateLocalVariables(w);
		b.generateCode(w," ");
		w.write(" }");
		w.newLine();
		w.write(" public static void main (String []args){");
		w.newLine();
		w.write("  "+i.toLowerCase()+"();");
		w.newLine();
		w.write(" }");
		w.newLine();
		w.write("}");
		
		w.newLine();
		w.flush();
		w.close();
		}catch (IOException e) {}
	}
}
