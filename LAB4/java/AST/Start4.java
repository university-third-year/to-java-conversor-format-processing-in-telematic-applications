package AST;
import Errors.*;
import java.io.*;

public class Start4 implements Start{

	public final String i;
	public final Body b;

	public Start4(String i, Body b){
		this.i=i;
		this.b=b;
	}
	public void computeAH1() throws CompilerExc{}
	public void computeStTyp() throws CompilerExc{
		b.computeStTyp();
	}
	
	public void generateCode() throws IOException{
		try {
		BufferedWriter w= new BufferedWriter(new FileWriter(i+".java"));
		w.write("import GeneratedCodeLib.*;");
		w.newLine();
		w.write("import java.util.*;");
		w.newLine();
		w.newLine();
		
		
		w.write("public class "+i+" {");
		w.newLine();
		w.write(" public static void "+i.toLowerCase()+" () {");
		w.newLine();
		b.generateCode(w," ");
		w.write(" }");
		w.newLine();
		w.write(" public static void main (String []args){");
		w.newLine();
		w.write("  "+i.toLowerCase()+"();");
		w.newLine();
		w.write(" }");
		w.newLine();
		w.write("}");
		
		w.newLine();
		w.flush();
		w.close();
		}catch (IOException e) {}
	}
}
