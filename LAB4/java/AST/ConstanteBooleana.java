package AST;
import Compiler.Typ;
import Errors.*;
import java.io.*;

public class ConstanteBooleana implements Exp{
	public final boolean c;

	public ConstanteBooleana(boolean c){
		this.c=c;
	}
	public int computeType() throws CompilerExc{
		return Typ.tbool;
	}
	
	public String generateCode() throws CompilerExc{
		return String.valueOf(c);
	}
	
}