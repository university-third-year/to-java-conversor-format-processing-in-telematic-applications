package AST;
import Errors.*;
import Compiler.*;
import java.io.*;

public class Resta implements Exp{

	public final Exp e1,e2;
	
	public Resta(Exp e1, Exp e2){
		this.e1=e1;
		this.e2=e2;
	}
	public int computeType() throws CompilerExc{
		if((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.tint)) {
			return Typ.tint;
		}else if(((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.tint))||((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.trat))
				||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.trat))) {
			return Typ.trat;
		}else {
			throw new TypeException("Error en resta: conflicto de tipo entre operandos.");
		}
	}
	public String generateCode() throws CompilerExc{
		if((e1.computeType()==Typ.trat)||(e2.computeType()==Typ.trat)) {
			return "Rational.calcRestaRat(" + e1.generateCode() + "," + e2.generateCode() + ")";
		}else {
			return e1.generateCode()+"-"+e2.generateCode(); 
		}
	}
}