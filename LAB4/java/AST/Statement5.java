package AST;
import java.io.*;
import Errors.*;
import Compiler.*;

public class Statement5 implements Statement{
	
	public final Exp e;
	public final StatementList sl;

	
	public Statement5(StatementList sl, Exp e){
		this.sl=sl;
		this.e=e;
	}
	public void computeStTyp() throws CompilerExc{
		if(e.computeType()!=Typ.tbool) {
			throw new TypeException("Error in repeat-until, la condición no es booleana.");
		}
		sl.computeStTyp();
	}
	
	public void generateCode(BufferedWriter w, String indent) throws IOException{
		try {
			indent = " "+indent;
			w.write(indent + "do{");
				w.newLine();
				sl.generateCode(w, indent);
				w.newLine();
			w.write(indent + "} while(!(" + e.generateCode() + "));");
			w.newLine();
		}catch(CompilerExc e) { }
	}
}