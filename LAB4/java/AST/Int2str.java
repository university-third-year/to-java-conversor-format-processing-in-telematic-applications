package AST;
import Compiler.Typ;
import Errors.*;

public class Int2str implements Exp{
	public final Exp e1;

	public Int2str(Exp e1){
		this.e1=e1;
	}
	public int computeType() throws CompilerExc{
		if(e1.computeType()==Typ.tint){
			return Typ.tstr;
		}else {
			throw new TypeException("Error en int2str: la expresion no es de tipo int.");
		}
	}
	public String generateCode() throws CompilerExc{
		return "String.valueOf("+e1.generateCode()+")";
	}
}