package AST;
import Compiler.Typ;
import Errors.*;

public class Denominador implements Exp{

	public final Exp e;

	public Denominador(Exp e){
		this.e=e;
	}
	public int computeType() throws CompilerExc{
		if(e.computeType()==Typ.trat) {
			return Typ.tint;
		}else {
			throw new TypeException("Error en denominador: la expresion no es de tipo rational");
		}
	}
	public String generateCode() throws CompilerExc{
		return "Rational.denominador("+e.generateCode()+")";
	}
}