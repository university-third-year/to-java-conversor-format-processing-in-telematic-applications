package AST;
import Errors.*;
import java.io.*;

public interface Decl{
	public void computeAH1() throws CompilerExc;
	public void generateLocalVariables(BufferedWriter w) throws IOException;
}