import GeneratedCodeLib.*;
import java.util.*;

public class MapNaturals2Rationals {
 public static void mapnaturals2rationals (int n) {
  int i;
  int j;
  int k;
  Rational d;
  boolean tocaNum;
  boolean nuevo;
  if(n > 0){
   d=Rational.calcDiv(0,1);
   System.out.println("Racional 1= "+Rational.q2str(d)+"~ "+Rational.q2strd(d,3)+";");

  }
  if(n > 1){
   d=Rational.calcDiv(1,1);
   System.out.println("Racional 2= "+Rational.q2str(d)+"~ "+Rational.q2strd(d,3)+";");

  }
  if(n > 2){
   i=2;
   k=2;
   tocaNum=true;
   nuevo=false;
   j=1;
   do{
    if(tocaNum){
     d=Rational.calcDiv(j,k);
     if(Rational.denominador(d) == k){
      nuevo=true;

     }

    } else{
     d=Rational.calcDiv(k,j);
     if(Rational.denominador(d) == j){
      nuevo=true;

     }

    }
    if(nuevo){
     i=i+1;
     System.out.println("Racional "+String.valueOf(i)+"= "+Rational.q2str(d)+"~ "+Rational.q2strd(d,3)+";");

    }
    nuevo=false;
    j=j+1;
    if(j == k){
     j=1;
     tocaNum=! tocaNum;
     if(tocaNum){
      k=k+1;

     }

    }

   } while(!(i == n));

  }
 }
 public static void main (String []args){
  int nVars=1;
  int[] params=new int[nVars];
  if(nVars<=args.length){
   for(int i=0;i<params.length;i++){
    params[i]=Integer.parseInt(args[i]);
   }
  }else{
   for(int i=0;i<args.length;i++){
    params[i] = Integer.parseInt(args[i]);
   }
   for(int i=args.length; i<nVars; i++){
    params[i] = 0;
   }
  }
  mapnaturals2rationals(params[0]);
 }
}
