import GeneratedCodeLib.*;
import java.util.*;

public class Pruebas {
 public static void pruebas () {
  int i;
  Rational e;
  Rational term;
  boolean b;
  i=1;
  e=Rational.calcDiv(1,1);
  term=Rational.calcDiv(1,1);
  do{
   i=i+1;
   e=Rational.calcOpuestoRat(Rational.calcDiv(e,- (i)));

  } while(!(Rational.igualQueRat(Rational.calcDiv(i,9999),Rational.calcDiv(17,9999))));
  System.out.println("Resultado 1= "+Rational.q2str(e));
  e=Rational.calcDiv(1,1);
  i=25;
  do{
   i=i-1;
   e=Rational.calcOpuestoRat(Rational.calcDiv(e,- (i)));

  } while(!(Rational.igualQueRat(Rational.calcDiv(i,9999),Rational.calcDiv(17,9999))));
  System.out.println("Resultado 2= "+Rational.q2str(e));
  System.out.println("Resultado 3= "+String.valueOf(Rational.parteEntera(e)));
  System.out.println("Resultado 4= "+String.valueOf(Rational.parteEntera(Rational.calcOpuestoRat(Rational.calcDiv(1,20)))));
  System.out.println("Resultado 5= "+String.valueOf(Rational.parteEntera(Rational.calcOpuestoRat(Rational.calcDiv(5,2)))));
  System.out.println("Resultado 6= "+String.valueOf(Rational.parteEntera(Rational.calcDiv(1,20))));
  b=Rational.igualQueRat(2,Rational.calcDiv(46,23));
  if(b){
   System.out.println("Resultado 7 correcto");

  }
  b=Rational.menorRat(Rational.calcDiv(23,46),2);
  if(b){
   System.out.println("Resultado 8 correcto");

  }
  b=true && b;
  if(b){
   System.out.println("Resultado 9 correcto");

  }
  b=false || ! b;
  if(! b){
   System.out.println("Resultado 10 correcto");

  }
 }
 public static void main (String []args){
  pruebas();
 }
}
