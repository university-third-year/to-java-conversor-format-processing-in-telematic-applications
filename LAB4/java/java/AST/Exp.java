package AST;
import Errors.*;
import java.io.*;

public interface Exp{
	public int computeType() throws CompilerExc;
	public String generateCode() throws CompilerExc;
}