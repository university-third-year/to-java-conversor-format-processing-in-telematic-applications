package AST;
import Errors.*;
import Compiler.*;

public class ConstanteString implements Exp{
	public final String s;

	public ConstanteString(String s){
		this.s=s;
	}
	public int computeType() throws CompilerExc{
		return Typ.tstr;
	}
	public String generateCode() throws CompilerExc{
		return s;
	}
}