package AST;
import Compiler.Typ;
import Errors.*;

public class IgualQue implements Exp{

	public final Exp e1,e2;

	public IgualQue(Exp e1, Exp e2){
		this.e1=e1;
		this.e2=e2;
	}
	public int computeType() throws CompilerExc{
		if(((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.tint))||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.trat))||((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.trat))||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.tint))
				||((e1.computeType()==Typ.tbool)&&(e2.computeType()==Typ.tbool))) {
			return Typ.tbool;
		}else {
			throw new TypeException("Error en igual que: conflicto de tipo entre operandos.");
		}
	}
	
	public String generateCode() throws CompilerExc{
		if((e1.computeType()==Typ.trat)||(e2.computeType()==Typ.trat)) {
			return "Rational.igualQueRat("+e1.generateCode()+","+e2.generateCode()+")";
		}else {
			return e1.generateCode()+ " == " +e2.generateCode();
		}
	}

}