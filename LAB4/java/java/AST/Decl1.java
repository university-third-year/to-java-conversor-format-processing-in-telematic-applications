package AST;
import Errors.*;
import java.io.*;

public class Decl1 implements Decl{

	public final int type;
	public final IdentList il;
	public Decl1(int type, IdentList il){
		this.type=type;
		this.il=il;
	}
	public void computeAH1() throws CompilerExc{
		il.computeAH1(type);
	}
	public void generateLocalVariables(BufferedWriter w) throws IOException{
		il.generateLocalVariables(w);
	}
}