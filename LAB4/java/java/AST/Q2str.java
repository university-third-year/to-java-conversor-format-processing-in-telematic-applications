package AST;
import Compiler.Typ;
import Errors.*;

public class Q2str implements Exp{
	public final Exp e;

	public Q2str(Exp e){
		this.e=e;
	}
	public int computeType() throws CompilerExc{
		if(e.computeType()==Typ.trat) {
			return Typ.tstr;
		}else {
			throw new TypeException("Error en q2str: la expresión no es de tipo rational.");
		}
	}
	public String generateCode() throws CompilerExc{
		return "Rational.q2str("+e.generateCode()+")";
	}
}