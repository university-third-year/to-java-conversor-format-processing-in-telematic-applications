package AST;
import Errors.*;
import Compiler.*;
import java.io.*;

public interface IdentList{
	public void computeAH1(int type) throws CompilerExc;
	public int getAH1();
	public String generateArguments();
	public void generateLocalVariables(BufferedWriter w) throws IOException;
	public int nVars(int n);
}