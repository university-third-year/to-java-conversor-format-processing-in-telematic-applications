package AST;
import Errors.*;
import java.io.*;

public class Start2 implements Start{
	public final String i;
	public final LDecl ld;
	public final Body b;
	
	public Start2(String i, LDecl ld, Body b){
		this.i=i;
		this.ld=ld;
		this.b=b;
	}
	public void computeAH1() throws CompilerExc{
		ld.computeAH1();
	}

	public void computeStTyp() throws CompilerExc{
		b.computeStTyp();
	}
	public void generateCode(BufferedWriter w) throws IOException{
		w.write(" public static void "+i+" () {");
		w.newLine();
		ld.generateLocalVariables(w);
		b.generateCode(w," ");
		w.write(" }");
		w.newLine();
		w.write(" public static void main (String []args){");
		w.newLine();
		w.write("  "+i+"();");
		w.newLine();
		w.write(" }");
		w.newLine();
	}
}
