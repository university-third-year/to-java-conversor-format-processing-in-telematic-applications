package AST;
import Errors.*;
import Compiler.*;
import java.io.*;

public class IdentList2 implements IdentList{

	public final IdentList il1,il2;
	private int ah1;

	public IdentList2(IdentList il1, IdentList il2){
		this.il1=il1;
		this.il2=il2;
	}
	public void computeAH1(int type) throws CompilerExc{
		ah1=type;
		il1.computeAH1(type);
		il2.computeAH1(type);
	}
	public int getAH1() {
		return ah1;
	}
	public void generateLocalVariables(BufferedWriter w) throws IOException{
		il1.generateLocalVariables(w);
		il2.generateLocalVariables(w);
	}
	public String generateArguments() {
		return il1.generateArguments() + "," + il2.generateArguments();
	}
	public int nVars(int n) {
		return il1.nVars(n)+il2.nVars(n);
	}
}