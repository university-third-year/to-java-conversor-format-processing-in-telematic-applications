package AST;
import Compiler.*;
import Errors.*;

public class Not implements Exp{
	public final Exp e1;

	public Not(Exp e1){
		this.e1=e1;
	}
	public int computeType() throws CompilerExc{
		if(e1.computeType()==Typ.tbool) {
			return Typ.tbool;
		}else {
			throw new TypeException("Error en not: la expresión no es de tipo bool.");
		}
	}
	
	public String generateCode() throws CompilerExc{
		return "! "+ e1.generateCode();
	}

}