package AST;
import Compiler.Typ;
import Errors.*;

public class And implements Exp{
	public final Exp e1, e2;
	

	public And(Exp e1, Exp e2){
		this.e1=e1;
		this.e2=e2;
	}
	public int computeType() throws CompilerExc{
		if((e1.computeType()==Typ.tbool)&&(e2.computeType()==Typ.tbool)) {
			return Typ.tbool;
		}else {
			throw new TypeException("Error en and: las expresiones no son de tipo bool.");
		}
	}
	public String generateCode() throws CompilerExc{ //harian falta paréntesis?
		return e1.generateCode() + " && " + e2.generateCode();
	}
}