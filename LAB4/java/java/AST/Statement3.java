package AST;
import Errors.*;
import java.io.*;
import Compiler.*;

public class Statement3 implements Statement{
	
	public final Exp e;
	public final StatementList sl;
	
	public Statement3(Exp e, StatementList sl){
		this.sl=sl;
		this.e=e;
	}
	public void computeStTyp() throws CompilerExc{
		if(e.computeType()!=Typ.tbool) {
			throw new TypeException("Error in if-then, la condición no es booleana.");
		}
		sl.computeStTyp();
	}
	public void generateCode(BufferedWriter w, String indent) throws IOException{
		try {
			indent = " "+indent;
			w.write(indent + "if(" + e.generateCode() + "){");
				w.newLine();
				sl.generateCode(w,indent);
				w.newLine();
			w.write(indent + "}");
			w.newLine();
		}catch(CompilerExc e) { }
	}

}