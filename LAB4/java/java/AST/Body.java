package AST;
import Errors.*;
import java.io.*;

public interface Body{
	public void computeStTyp() throws CompilerExc;
	public void generateCode(BufferedWriter w, String indent) throws IOException;
}