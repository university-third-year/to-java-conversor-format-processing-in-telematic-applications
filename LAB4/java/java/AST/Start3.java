package AST;
import Errors.*;
import Compiler.*;
import java.io.*;

public class Start3 implements Start{

	public final String i;
	public final IdentList il;
	public final Body b;
	
	public Start3(String i, IdentList il, Body b){
		this.i=i;
		this.il=il;
		this.b=b;
	}
	public void computeAH1() throws CompilerExc{
		il.computeAH1(Typ.tint);
	}
	public void computeStTyp() throws CompilerExc{
		b.computeStTyp();
	}
	
	public void generateCode(BufferedWriter w) throws IOException{
		int nVars=il.nVars(0);
		w.write(" public static void "+i+" ("+il.generateArguments()+") {");
		w.newLine();
		b.generateCode(w," ");
		w.write(" }");
		w.newLine();
		w.write(" public static void main (String []args){");
		w.newLine();
		w.write("  int nVars="+String.valueOf(nVars)+";");
		w.newLine();
		w.write("  int[] params=new int[nVars];");
		w.newLine();
		w.write("  if(nVars<=args.length){");
		w.newLine();
		w.write("   for(int i=0;i<params.length;i++){");
		w.newLine();
		w.write("    params[i]=Integer.parseInt(args[i]);");
		w.newLine();
		w.write("   }");
		w.newLine();
		w.write("  }else{");
		w.newLine();
		w.write("   for(int i=0;i<args.length;i++){");
		w.newLine();
		w.write("    params[i] = Integer.parseInt(args[i]);");
		w.newLine();
		w.write("  }");
		w.newLine();
		w.write("   for(int i=args.length; i<nVars; i++){");
		w.newLine();
		w.write("    params[i] = 0;");
		w.newLine();
		w.write("   }");
		w.newLine();
		w.write("  }");
		w.newLine();
		w.write("  "+i+"(");
		for(int i=0;i<nVars;i++) {
			if(i==nVars-1) {
				w.write("params["+String.valueOf(i)+"]");
			}else {
				w.write("params["+String.valueOf(i)+"],");
			}
		}
		w.write(");");
		w.newLine();
		w.write(" }");
		w.newLine();
	}
}