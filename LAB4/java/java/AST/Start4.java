package AST;
import Errors.*;
import java.io.*;

public class Start4 implements Start{

	public final String i;
	public final Body b;

	public Start4(String i, Body b){
		this.i=i;
		this.b=b;
	}
	public void computeAH1() throws CompilerExc{}
	public void computeStTyp() throws CompilerExc{
		b.computeStTyp();
	}
	
	public void generateCode(BufferedWriter w) throws IOException{
		w.write(" public static void "+i+" () {");
		w.newLine();
		b.generateCode(w," ");
		w.write(" }");
		w.newLine();
		w.write(" public static void main (String []args){");
		w.newLine();
		w.write("  "+i+"();");
		w.newLine();
		w.write(" }");
		w.newLine();
	}
}
