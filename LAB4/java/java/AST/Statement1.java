package AST;
import java.io.*;
import Errors.*;
import Compiler.*;

import java.io.BufferedWriter;

import Compiler.*;

public class Statement1 implements Statement{


	public final String i;
	public final Exp e;
	
	public Statement1(String i,Exp e){
		this.i=i;
		this.e=e;
	}
	public void computeStTyp() throws CompilerExc{
		if(SymbolTable.getType(i)!=e.computeType()) {
			throw new TypeException("Error in assign");
		}
	}
	
	public void generateCode(BufferedWriter w, String indent) throws IOException{
		try{
		indent = " "+indent;
		w.write(indent + i + "=" + e.generateCode() + ";"); 
		w.newLine();	
		}catch(CompilerExc e) { }
	}
}