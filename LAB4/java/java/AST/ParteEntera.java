package AST;
import Errors.*;
import Compiler.*;

public class ParteEntera implements Exp{

	public final Exp e;

	public ParteEntera(Exp e){
		this.e=e;
	}


	public int computeType() throws CompilerExc{
		if(e.computeType()==Typ.trat) {
			return Typ.tint;
		}else {
			throw new TypeException("Error en parte.entera: la expresión no es de tipo rational.");
		}
	}
	public String generateCode() throws CompilerExc{
		return "Rational.parteEntera("+e.generateCode()+")";
	}
}