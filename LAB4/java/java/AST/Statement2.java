package AST;
import java.io.*;
import Errors.*;
import Compiler.*;

public class Statement2 implements Statement{
	public final Exp e;
	
	public Statement2(Exp e){
		this.e=e;
	}
	public void computeStTyp() throws CompilerExc{
		if(e.computeType()!=Typ.tstr) {
			throw new TypeException("Error in print, la expresión no es de tipo string");
		}
	}
	
	public void generateCode(BufferedWriter w, String indent) throws IOException{
		try {	
			indent = " "+indent;
			w.write(indent + "System.out.println(" + e.generateCode() + ");");
			w.newLine();
		}catch(CompilerExc e) { }
	}
}