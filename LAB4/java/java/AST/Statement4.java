package AST;
import java.io.*;
import Errors.*;
import Compiler.*;

public class Statement4 implements Statement{
	
	public final Exp e;
	public final StatementList sl1, sl2;

	
	public Statement4(Exp e, StatementList sl1, StatementList sl2){
		this.sl1=sl1;
		this.sl2=sl2;
		this.e=e;
	}

	public void computeStTyp() throws CompilerExc{
		
		if(e.computeType()!=Typ.tbool) {
			throw new TypeException("Error in if-then-else, la condición no es booleana.");
		}
		sl1.computeStTyp();
		sl2.computeStTyp();
	}
	
	public void generateCode(BufferedWriter w, String indent) throws IOException{
		try {
			indent = " "+indent;
			w.write(indent + "if(" + e.generateCode() + "){");
				w.newLine();
				sl1.generateCode(w,indent);
				w.newLine();
			w.write(indent + "} else{");
				w.newLine();
				sl2.generateCode(w,indent);
				w.newLine();
			w.write(indent + "}");
			w.newLine();
		}catch(CompilerExc e) { }
	}
	
}