package AST;
import java.io.*;

import Errors.*;

public class LDecl1 implements LDecl{

	public final Decl d;
	
	public LDecl1(Decl d){
		this.d=d;
	}

	public void computeAH1() throws CompilerExc{
		d.computeAH1();
	}
	public void generateLocalVariables(BufferedWriter w) throws IOException{
		d.generateLocalVariables(w);
	}
}