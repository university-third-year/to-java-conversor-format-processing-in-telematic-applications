package AST;
import java.io.*;
import java.io.BufferedWriter;

import Errors.*;

public class LDecl2 implements LDecl{

	public final Decl d;
	public final LDecl ld;
	
	public LDecl2(Decl d, LDecl ld){
		this.d=d;
		this.ld=ld;
	}
	public void computeAH1() throws CompilerExc{
		d.computeAH1();
		ld.computeAH1();
	}
	
	public void generateLocalVariables(BufferedWriter w) throws IOException{
		d.generateLocalVariables(w);
		ld.generateLocalVariables(w);
	}
}