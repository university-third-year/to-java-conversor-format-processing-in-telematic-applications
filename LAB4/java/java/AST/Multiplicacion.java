package AST;
import java.io.*;
import Compiler.*;
import Errors.*;

public class Multiplicacion implements Exp{

	public final Exp e1,e2;

	public Multiplicacion(Exp e1, Exp e2){
		this.e1=e1;
		this.e2=e2;
	}
	public int computeType() throws CompilerExc{
		if((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.tint)) {
			return Typ.tint;
		}else if(((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.tint))||((e1.computeType()==Typ.tint)&&(e2.computeType()==Typ.trat))
				||((e1.computeType()==Typ.trat)&&(e2.computeType()==Typ.trat))) {
			return Typ.trat;
		}else {
			throw new TypeException("Error en multiplicacion: conflicto de tipo entre operandos.");
		}
	}
	public String generateCode() throws CompilerExc{
		if((e1.computeType()==Typ.trat)||(e2.computeType()==Typ.trat)) {
			return "Rational.calcMultRat("+e1.generateCode()+","+e2.generateCode()+")";
		}else {
			return "("+e1.generateCode()+")*("+e2.generateCode()+")";
		}
	}
}