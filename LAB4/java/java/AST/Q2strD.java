package AST;
import Compiler.Typ;
import Errors.*;

public class Q2strD implements Exp{
	public final Exp e;
	public final int n;

	public Q2strD(Exp e, int n){
		this.e=e;
		this.n=n;
	}
	public int computeType() throws CompilerExc{
		if(e.computeType()==Typ.trat) {
			return Typ.tstr;
		}else {
			throw new TypeException("Error en q2str.decimal: la expresión no es de tipo rational.");
		}
	}
	
	public String generateCode() throws CompilerExc {
		return "Rational.q2strd(" + e.generateCode() + "," + String.valueOf(n)+ ")";
	}
}