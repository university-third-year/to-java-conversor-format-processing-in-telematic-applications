package Compiler;

public class Typ{
	public static final int tint=0;
	public static final int tbool=1;
	public static final int tstr=2;
	public static final int trat=3;

	public static String typToString(int  typ){
		if(typ==tint){ 
			return "int";
		} else if(typ==tbool){
			return "bool";
		} else if(typ==tstr){
			return "str";
		}
		else{
			return "rational";
		}
	}

}
