package GeneratedCodeLib;
import java.math.BigInteger;
public class Rational{
	private int pe;
	private BigInteger num;
	private BigInteger den;
	
	public Rational () {}
	
	public int getPe() {return pe;}
	public BigInteger getNum(){return num;}
	public BigInteger getDen(){return den;}
	public void setPe(int pe) {this.pe=pe;}
	public void setNum(BigInteger num) {this.num=num;}
	public void setDen(BigInteger den) {this.den=den;}
	
	public static Rational calcDiv(int a, int b) {
		Rational auxR=new Rational();
		BigInteger[] aux = {(BigInteger.valueOf(a)).abs(),(BigInteger.valueOf(b)).abs(),BigInteger.valueOf(0)};
		BigInteger gcd=BigInteger.valueOf(0);
		BigInteger num_aux=BigInteger.valueOf(0);
		boolean esReducible=true;
		
		if(((a>=0)&&(b>=0))||((a<=0)&&(b<=0))) {
		aux[2]=aux[0].divide(aux[1]);
		
		num_aux=aux[0].subtract(aux[2].multiply(aux[1]));
		aux[0]=num_aux;
		
		if(aux[0].intValue()==0) {
			aux[1]=BigInteger.valueOf(1);
			auxR.setPe(aux[2].intValue());
			auxR.setNum(aux[0]);
			auxR.setDen(aux[1]);
			return auxR;
		}
		while(esReducible) {
			gcd=aux[0].gcd(aux[1]);
			if(gcd.intValue()!=1) {

				aux[0]=aux[0].divide(gcd);
				aux[1]=aux[1].divide(gcd);
			}else {
				esReducible=false;
			}
		}
	}else {
		aux[2]=(aux[0].divide(aux[1]));//.subtract(BigInteger.valueOf(1));
		num_aux=aux[0].subtract(aux[2].multiply(aux[1]));
		num_aux=aux[1].subtract(num_aux);
		aux[0]=num_aux;
		
		if(aux[0].intValue()==0) {
			aux[1]=BigInteger.valueOf(1);
			aux[2]=aux[2].negate();
			auxR.setPe(aux[2].intValue());
			auxR.setNum(aux[0]);
			auxR.setDen(aux[1]);
			return auxR;
		}
		
		while(esReducible) {
			gcd=aux[0].gcd(aux[1]);
			if(gcd.intValue()!=1) {

				aux[0]=aux[0].divide(gcd);
				aux[1]=aux[1].divide(gcd);
			}else {
				esReducible=false;
			}
		}
		aux[2]=(aux[2].negate()).subtract(BigInteger.valueOf(1));
	}
		auxR.setPe(aux[2].intValue());
		auxR.setNum(aux[0]);
		auxR.setDen(aux[1]);
		return auxR;
	}
	
	public static Rational calcDiv(Rational a, Rational b) {
		BigInteger num1=((BigInteger.valueOf(a.getPe())).multiply(a.getDen())).add(a.getNum());
		BigInteger den1=a.getDen();
		BigInteger num2=((BigInteger.valueOf(b.getPe())).multiply(b.getDen())).add(b.getNum());
		BigInteger den2=b.getDen();
		
		return calcDiv((num1.multiply(den2)).intValue(),(num2.multiply(den1)).intValue());
	}
	
	public static Rational calcDiv(int a, Rational b) {
		Rational aRat=intToRat(a);
		
		return calcDiv(aRat,b);
	}
	public static Rational calcDiv(Rational a, int b) {
		Rational bRat=intToRat(b);
		
		return calcDiv(a,bRat);
	}
	
	public static Rational calcSumaResRat(Rational a, Rational b) {
		BigInteger num1=((BigInteger.valueOf(a.getPe())).multiply(a.getDen())).add(a.getNum());
		BigInteger den1=a.getDen();
		BigInteger num2=((BigInteger.valueOf(b.getPe())).multiply(b.getDen())).add(b.getNum());
		BigInteger den2=b.getDen();
		
		BigInteger num=(num1.multiply(den2)).add(num2.multiply(den1));
		
		return calcDiv(num.intValue(),(den2.multiply(den1)).intValue());
	}
	public static Rational calcSumaResRat(int a, Rational b) {
		Rational aRat=intToRat(a);
		
		return calcSumaResRat(aRat,b);
	}
	
	public static Rational calcSumaResRat(Rational a,int  b) {
		Rational bRat=intToRat(b);
		
		return calcSumaResRat(a,bRat);
	}
	
	public static Rational calcMultRat(Rational a, Rational b) {
		BigInteger num1=((BigInteger.valueOf(a.getPe())).multiply(a.getDen())).add(a.getNum());
		BigInteger den1=a.getDen();
		BigInteger num2=((BigInteger.valueOf(a.getPe())).multiply(b.getDen())).add(b.getNum());
		BigInteger den2=b.getDen();
		
		return calcDiv((num1.multiply(num2)).intValue(),(den1.multiply(den2)).intValue());

	}
	public static Rational calcMultRat(int a, Rational b) {
		Rational aRat=intToRat(a);
		
		return calcMultRat(aRat,b);
	}
	public static Rational calcMultRat(Rational a, int b) {
		Rational bRat=intToRat(b);
		
		return calcMultRat(a,bRat);

	}

	public static Rational calcOpuestoRat(Rational a) {
		BigInteger num=(((BigInteger.valueOf(a.getPe())).multiply(a.getDen())).add(a.getNum())).abs();
		BigInteger den=a.getDen();
		
		return calcDiv(num.intValue(),den.intValue());

	}
	public static Rational intToRat(int a) {
		Rational aux=new Rational();
		aux.setPe(a);
		aux.setNum(BigInteger.valueOf(0));
		aux.setDen(BigInteger.valueOf(1));
		return aux;
	}
	public static String q2str(Rational a) {
		return String.valueOf(a.getPe())+"+"+String.valueOf((a.getNum()).intValue())+"/"+String.valueOf(a.getDen().intValue());
	}
	public static String q2strd(Rational a, int n) {
		int pe = a.getPe();
		int num = a.getNum().intValue();
		int den = a.getDen().intValue();
		double d =(double) (pe*den+num)/(den);
		
		String sd=String.format("%.20f",d);
		String s=String.format("%.0f",d);
		sd=sd.substring(0,s.length()+n+1);
		return sd;
	}
	
	public static boolean mayorRat(Rational a,Rational b) {
		double da = Double.parseDouble(q2strd(a,20).replace(",","."));
		double db = Double.parseDouble(q2strd(b,20).replace(",","."));

		return da > db;
	}
	public static boolean mayorRat(int a,Rational b) {
		Rational arat = intToRat(a);

		return mayorRat(arat,b);
	}
	public static boolean mayorRat(Rational a,int b) {
		Rational brat = intToRat(b);

		return mayorRat(a,brat);
	}
	
	public static boolean menorRat(Rational a,Rational b) {
		double da = Double.parseDouble(q2strd(a,20).replace(",","."));
		double db = Double.parseDouble(q2strd(b,20).replace(",","."));

		return da < db;
	}
	public static boolean menorRat(int a,Rational b) {
		Rational arat = intToRat(a);

		return menorRat(arat,b);
	}
	public static boolean menorRat(Rational a,int b) {
		Rational brat = intToRat(b);

		return menorRat(a,brat);
	}
	
	public static boolean igualQueRat(Rational a,Rational b) {
		
		double da = Double.parseDouble(q2strd(a,20).replace(",","."));
		double db = Double.parseDouble(q2strd(b,20).replace(",","."));

		return da == db;
	}
	public static boolean igualQueRat(int a,Rational b) {
		Rational arat = intToRat(a);

		return igualQueRat(arat,b);
	}
	public static boolean igualQueRat(Rational a,int b) {
		Rational brat = intToRat(b);

		return igualQueRat(a,brat);
	}
	
	
	public static int numerador(Rational a) {
		return a.getNum().intValue();
	}
	
	public static int denominador(Rational a) {
		return a.getDen().intValue();
	}
	
	public static int parteEntera(Rational a) {
		return a.getPe();
	}
	
	
	
	
	
}