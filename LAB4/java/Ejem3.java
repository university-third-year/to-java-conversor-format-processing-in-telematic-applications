import GeneratedCodeLib.*;
import java.util.*;

public class Ejem3 {
 public static void ejem3 (int n) {
  int i;
  int j;
  int k;
  Rational d;
  Rational acum;
  Rational alt;
  Rational coc;
  boolean tocaNum;
  boolean nuevo;
  acum=Rational.calcDiv(0,1);
  alt=Rational.calcDiv(0,1);
  coc=Rational.calcDiv(1,1);
  if(n > 2){
   i=2;
   k=2;
   tocaNum=true;
   nuevo=false;
   j=1;
   do{
    if(tocaNum){
     d=Rational.calcDiv(j,k);
     if(Rational.denominador(d) == k){
      nuevo=true;

     }

    } else{
     d=Rational.calcDiv(k,j);
     if(Rational.denominador(d) == j){
      nuevo=true;

     }

    }
    if(nuevo){
     i=i+1;
     System.out.println("Racional "+String.valueOf(i)+"= "+Rational.q2str(d)+"~ "+Rational.q2strd(d,3)+";");
     acum=Rational.calcSumaRat(acum,d);
     coc=Rational.calcDiv(coc,Rational.calcMultRat(d,d));
     if(Rational.numerador(Rational.calcDiv(i,2)) == 0){
      alt=Rational.calcSumaRat(alt,d);

     } else{
      alt=Rational.calcRestaRat(alt,d);

     }
     System.out.println("Acumulado= "+Rational.q2str(acum)+";");
     System.out.println("alt= "+Rational.q2str(alt)+";");
     System.out.println("coc= "+Rational.q2str(coc)+";");

    }
    nuevo=false;
    j=j+1;
    if(j == k){
     j=1;
     tocaNum=! tocaNum;
     if(tocaNum){
      k=k+1;

     }

    }

   } while(!(i == n));

  }
 }
 public static void main (String []args){
  int nVars=1;
  int[] params=new int[nVars];
  if(nVars<=args.length){
   for(int i=0;i<params.length;i++){
    params[i]=Integer.parseInt(args[i]);
   }
  }else{
   for(int i=0;i<args.length;i++){
    params[i] = Integer.parseInt(args[i]);
   }
   for(int i=args.length; i<nVars; i++){
    params[i] = 0;
   }
  }
  ejem3(params[0]);
 }
}
